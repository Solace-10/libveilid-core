#[repr(C)]
#[allow(dead_code)]
pub enum VeilidConfigLogLevel {
    Off,
    Error,
    Warn,
    Info,
    Debug,
    Trace,
}

#[repr(C)]
#[allow(dead_code)]
pub enum VeilidDHTSchema {
    DFLT,
    SMPL,
    Invalid
}

impl VeilidDHTSchema {
    pub fn from(source: veilid_core::DHTSchema) -> VeilidDHTSchema {
        match source {
            veilid_core::DHTSchema::DFLT(_) => VeilidDHTSchema::DFLT,
            veilid_core::DHTSchema::SMPL(_) => VeilidDHTSchema::SMPL
        }
    }
}

#[repr(C)]
pub struct VeilidCryptoKey {
    bytes: [u8; 32]
}

impl VeilidCryptoKey {
    pub fn new(source: veilid_core::CryptoKey) -> Self {
        Self {
            bytes: source.bytes
        }
    }

    pub fn empty() -> Self {
        Self {
            bytes: [0; veilid_core::CRYPTO_KEY_LENGTH]
        }
    }
}

#[repr(C)]
pub struct VeilidOptionalCryptoKey {
    crypto_key: VeilidCryptoKey,
    valid: bool
}

impl VeilidOptionalCryptoKey {
    pub fn new(
        source: Option<&veilid_core::CryptoKey>
    ) -> Self {
        let crypto_key = match source {
            Some(key) => VeilidCryptoKey::new(key.clone()),
            None => VeilidCryptoKey::empty(),
        };

        Self {
            crypto_key,
            valid: source.is_some(),
        }
    }

    pub fn empty() -> Self {
        Self {
            crypto_key: VeilidCryptoKey::empty(),
            valid: false
        }
    }
}

#[repr(C)]
pub struct VeilidFourCC {
    bytes: [u8; 4]
}

impl VeilidFourCC {
    pub fn new(
        source: veilid_core::FourCC
    ) -> Self {
        Self {
            bytes: source.0
        }
    }

    pub fn empty() -> Self {
        Self {
            bytes: [0, 0, 0, 0]
        }
    }
}

#[repr(C)]
pub struct VeilidTypedKey {
    crypto_key: VeilidCryptoKey,
    fourcc: VeilidFourCC
}

impl VeilidTypedKey {
    pub fn new(
        source: veilid_core::TypedKey
    ) -> Self {
        Self {
            crypto_key: VeilidCryptoKey::new(source.value),
            fourcc: VeilidFourCC::new(source.kind)
        }
    }

    pub fn empty() -> Self {
        Self {
            crypto_key: VeilidCryptoKey::empty(),
            fourcc: VeilidFourCC::empty()
        }
    }
}

#[repr(C)]
pub struct VeilidDHTRecordDescriptor {
    key: VeilidTypedKey,
    owner: VeilidCryptoKey,
    owner_secret: VeilidOptionalCryptoKey,
    schema: VeilidDHTSchema
}

impl VeilidDHTRecordDescriptor {
    pub fn new(
        source: veilid_core::DHTRecordDescriptor
    ) -> Self {
        Self {
            key: VeilidTypedKey::new(source.key().clone()),
            owner: VeilidCryptoKey::new(source.owner().clone()),
            owner_secret: VeilidOptionalCryptoKey::new(source.owner_secret().clone()),
            schema: VeilidDHTSchema::from(source.schema().clone())
        }
    }

    pub fn empty() -> Self {
        Self {
            key: VeilidTypedKey::empty(),
            owner: VeilidCryptoKey::empty(),
            owner_secret: VeilidOptionalCryptoKey::empty(),
            schema: VeilidDHTSchema::Invalid
        }
    }
}

#[repr(C)]
pub struct VeilidValueData {
    seq: u32,
    data: ffi_support::ByteBuffer,
    writer: VeilidCryptoKey,
}

unsafe impl Send for VeilidValueData {}

impl VeilidValueData {
    pub fn new(
        source: &veilid_core::ValueData
    ) -> Self {
        Self {
            seq: source.seq(),
            data: ffi_support::ByteBuffer::from_vec(source.data().to_vec()),
            writer: VeilidCryptoKey::new(source.writer().clone())
        }
    }

    pub fn empty() -> Self {
        Self {
            seq: 0,
            data: ffi_support::ByteBuffer::from_vec(Vec::new()),
            writer: VeilidCryptoKey::empty()
        }
    }
}

#[repr(C)]
pub struct VeilidOptionalValueData {
    value_data: VeilidValueData,
    valid: bool
}

impl VeilidOptionalValueData {
    pub fn new(
        source: Option<&veilid_core::ValueData>
    ) -> Self {
        let value_data = match source {
            Some(key) => VeilidValueData::new(key),
            None => VeilidValueData::empty(),
        };

        Self {
            value_data,
            valid: source.is_some(),
        }
    }

    pub fn empty() -> Self {
        Self {
            value_data: VeilidValueData::empty(),
            valid: false
        }
    }
}

#[repr(C)]
pub struct VeilidRouteBlob {
    pub route_id: VeilidCryptoKey,
    pub blob: ffi_support::ByteBuffer
}

unsafe impl Send for VeilidRouteBlob {}

impl VeilidRouteBlob {
    pub fn new(
        route_id: veilid_core::CryptoKey,
        blob: Vec<u8>
    ) -> Self {
        Self {
            route_id: VeilidCryptoKey::new(route_id),
            blob: blob.into()
        }
    }

    pub fn empty() -> Self {
        Self {
            route_id: VeilidCryptoKey::empty(),
            blob: ffi_support::ByteBuffer::from_vec(Vec::new())
        }
    }
}