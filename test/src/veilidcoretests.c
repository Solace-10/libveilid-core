#include <stdio.h>
#include <stdlib.h>
#include "veilidcore.h"

static const char* platform_config = 
"{\n"
"  \"logging\": {\n"
"    \"terminal\": {\n"
"      \"enabled\": true,\n"
"      \"level\": \"Info\"\n"
"    },\n"
"    \"otlp\": {\n"
"      \"enabled\": false,\n"
"      \"level\": \"Info\",\n"
"      \"grpc_endpoint\": \"none\",\n"
"      \"service_name\": \"none\"\n"
"    },\n"
"    \"api\": {\n"
"      \"enabled\": false,\n"
"      \"level\": \"Info\"\n"
"    }\n"
"  }\n"
"}";

char* read_file(const char* filename) 
{
    FILE* file = fopen(filename, "r");
    if (file == NULL) 
    {
        perror("Error opening file");
        return NULL;
    }

    // Get the file size
    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    // Allocate memory for the file contents
    char* contents = (char*)malloc(file_size + 1);
    if (contents == NULL) 
    {
        perror("Error allocating memory");
        fclose(file);
        return NULL;
    }

    // Read the file into the contents buffer
    size_t bytes_read = fread(contents, 1, file_size, file);
    if (bytes_read != file_size) 
    {
        perror("Error reading file");
        free(contents);
        fclose(file);
        return NULL;
    }

    // Null-terminate the string
    contents[file_size] = '\0';

    // Close the file and return the contents
    fclose(file);
    return contents;
}

bool veilid_callback_completed = false;
VeilidResult veilid_result = VEILID_RESULT_NOT_INITIALIZED;

void wait_for_callback()
{
    while (!veilid_callback_completed)
    {

    }
}

#define VEILID_TEST_ASYNC(description, function) \
    printf("%s: ", description);           \
    veilid_callback_completed = false;     \
    function;                              \
    wait_for_callback();                   \
    if (veilid_result == VEILID_RESULT_OK) \
    {                                      \
        printf("OK\n");                    \
    }                                      \
    else                                   \
    {                                      \
        printf("FAILED [error code: %d]\n", (int)veilid_result); \
        exit(-1);                          \
    }

void veilid_update(const char* data)
{
    
}

void veilid_generic_callback(VeilidResult result)
{
    veilid_result = result;
    veilid_callback_completed = true;
}

int main()
{
    printf("veilid_version_string(): %s\n", veilid_version_string());

    VeilidVersion version = veilid_version();
    printf("veilid_version(): %d.%d.%d\n", version.major, version.minor, version.patch);

    printf("veilid_core_initialize()... ");
    veilid_core_initialize(platform_config);
    printf("OK\n");

    printf("Reading veilid-config: ");
    char* veilid_config_contents = read_file("../test/bin/veilid-config.json");
    if (veilid_config_contents == NULL)
    {
        printf("FAILED\n");
        return -1;
    }
    else
    {
        printf("OK\n");
    }

    VEILID_TEST_ASYNC("veilid_core_startup", veilid_core_startup(&veilid_generic_callback, veilid_config_contents, &veilid_update));
    VEILID_TEST_ASYNC("veilid_attach", veilid_attach(&veilid_generic_callback));
    VEILID_TEST_ASYNC("veilid_detach", veilid_detach(&veilid_generic_callback));
    VEILID_TEST_ASYNC("veilid_core_shutdown", veilid_core_shutdown(&veilid_generic_callback));

    free(veilid_config_contents);

    return 0; 
}